from django.db import models

# Create your models here.

class Student(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    nama = models.CharField(max_length=200)
    nim = models.CharField(max_length=20)

    def __unicode__(self):
        return '{}-{}'.format(self.nama, self.nim)
